from pathlib import Path
import json

thisPath = Path('.')
currentPath = Path('.')
default = thisPath/'config.json'
customFile = thisPath/"custom.json"

#config class
class Configuration:
	def __init__(self, fname, lname, address):
		self.fname = fname
		self.lname = lname
		self.address = address

def writeFile(data, file):
	with open(file, 'w') as outfile:
		json.dump(data.__dict__, outfile)
		return('custom settings saved')

def readFile(file):
	with open(file, 'r') as f:
		data = json.load(f)
		#for key in data:
	return data #	print(f'{key} : {data[key]}')

def printFileInfo(data):
	for key in data:
		print(f'{key}: {data[key]}')

def settings():
	print("Settings ")

	change = input("Reset settings? ")

	if change in ('yes'):
		Path.unlink(customFile)
		print('default settings restored')
		save = input("Press any key to continue")
		


def main():
	if (customFile.exists() == False):
		usrPref = input("Y to use default settings, N to use custom settings: ")
		if (str(usrPref).lower() in ('yes')):
			if(default.exists() == False):
				print('Default settings not found: create default settings')
				fname = input("Enter first name: ")
				lname = input("Enter last name: ")
				addr = input("Enter address name: ")
				data = Configuration(fname, lname, addr)
				writeFile(data, default)
			settingsInfo = readFile(default)
			printFileInfo(settingsInfo)
		else:
			usrPref = input("Custom settings not found. Create custom settings: ")

			if str(usrPref).lower() in ('yes'):
				print("Great! Let's get you setup")

				fname = input("Enter first name: ")
				lname = input("Enter last name: ")
				addr = input("Enter address name: ")

				custom = Configuration(fname, lname, addr)
				# print(json.dumps(custom.__dict__))
				writeFile(custom, customFile)
				print('Custom settings saved')
	else:
		try:
			print(customFile.exists())
			print("loading custom settings")
			settingsInfo = readFile(customFile)
			printFileInfo(settingsInfo)
		except:
			print('Custom file not found')
# main()
if __name__ == "__main__":
	action = None
	while (action not in ('exit', 'quit')):
		print("\n***Type help for settings, enter to continue, exit to quit")
		action = input()
		if action == '' or action == '':
			main()
		elif action in ('quit, exit'):
			break;
		elif action in ('help') and action not in (""):
			settings()

